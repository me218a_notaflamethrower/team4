/****************************************************************************

  Header file for GunLED_ Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMGunLED__H
#define FSMGunLED__H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitGunState, GunLED_OFF, GunLED_PulsingON,
  GunLED_PulsingOFF
}GunLED_State_t;

// Public Function Prototypes

bool InitGunLED_FSM(uint8_t Priority);
bool PostGunLED_FSM(ES_Event_t ThisEvent);
ES_Event_t RunGunLED_FSM(ES_Event_t ThisEvent);
GunLED_State_t QueryGunLED_SM(void);

#endif /* FSMGunLED__H */

