/****************************************************************************

  Header file for GunDebounce Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMGunDebounce_H
#define FSMGunDebounce_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitGDState, GunReadyToShoot, Trigger_Debouncing
}GunDebounceState_t;

// Public Function Prototypes

bool InitGunDebounceFSM(uint8_t Priority);
bool PostGunDebounceFSM(ES_Event_t ThisEvent);
ES_Event_t RunGunDebounceFSM(ES_Event_t ThisEvent);
GunDebounceState_t QueryGunDebounceSM(void);
bool CheckForGunTrigger(void);
#endif /* FSMGunDebounce_H */

