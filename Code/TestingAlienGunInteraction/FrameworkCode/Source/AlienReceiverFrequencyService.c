/****************************************************************************
 Module
   AlienReceiverFrequencyService.c

 Revision
   1.0.1

 Description
   This is a AlienReceiverFrequency file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from AlienReceiverFrequencyFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "AlienReceiverFrequencyService.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
/*----------------------------- Module Defines ----------------------------*/
#define COUNT_WINDOW 1
#define PULSE_FREQUENCY 50
#define DELTA PULSE_FREQUENCY/10

//Setting ALIEN_RECEIVER_PIN at PF0
#define ALIEN_RECEIVER_PIN_HIGH BIT0HI
#define ALIEN_RECEIVER_PIN_LOW BIT0LO
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint8_t count, sum, LastInputState ;
static uint16_t TimeOfLastRise;
static bool IsFirstPulse;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitAlienReceiverFrequencyService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitAlienReceiverFrequencyService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  //Initialize PA5 to be ALIEN_RECEIVER_PIN
  HWREG(SYSCTL_RCGCGPIO) |= BIT5HI;
  while((HWREG(SYSCTL_PRGPIO) & BIT5HI) != BIT5HI)
  {
  }
  HWREG(GPIO_PORTF_BASE+GPIO_O_DEN) |= ALIEN_RECEIVER_PIN_HIGH;
  HWREG(GPIO_PORTF_BASE+GPIO_O_DIR) &= ALIEN_RECEIVER_PIN_LOW;
  //Initialize the value at PA5 to be LOW
  LastInputState = HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) & ALIEN_RECEIVER_PIN_HIGH;
  
  sum = 0;
  count = 0;
  IsFirstPulse = true;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostAlienReceiverFrequencyService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostAlienReceiverFrequencyService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunAlienReceiverFrequencyService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunAlienReceiverFrequencyService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ES_Event_t PostEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  uint16_t CurrentRiseTime;
  uint8_t average;
  uint16_t freq;
  
  if(ThisEvent.EventType == RISING_EDGE){
    if(IsFirstPulse){
      TimeOfLastRise = ThisEvent.EventParam;
      printf("First edge detected\r\n");
      IsFirstPulse = false;
    }
    else{
      printf("Future edges detected\r\n");
      CurrentRiseTime = ThisEvent.EventParam;
      freq = (976/(CurrentRiseTime - TimeOfLastRise));
      printf("%u\r\n", freq);
      //sum = sum + (1/(CurrentRiseTime - TimeOfLastRise));
      //count++;
      TimeOfLastRise = CurrentRiseTime;
      //if (count > COUNT_WINDOW){
        //average = sum/count;
      if((freq < (PULSE_FREQUENCY + DELTA)) && (freq > (PULSE_FREQUENCY - DELTA))){
          printf("Alien shot detected \r\n");
          PostEvent.EventType = ALIEN_SHOT;
          //ES_PostAll(PostEvent);
       // }
      }
      IsFirstPulse = true;
      sum = 0;
      count = 0;
    }
  }
  return ReturnEvent;
}

/*****************************************************************************
Function
    CheckForRisingEdges

 Parameters
   none

 Returns
   True if event detected

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
bool CheckForRisingEdge(void){
bool ReturnVal = false;
  uint8_t CurrentInputState;
  ES_Event_t ThisEvent;
  CurrentInputState = HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) & ALIEN_RECEIVER_PIN_HIGH;
  if(CurrentInputState != LastInputState){
    if(CurrentInputState != 0){
      //printf("Rising edge detected\r\n");
      ThisEvent.EventType = RISING_EDGE;
      ThisEvent.EventParam = ES_Timer_GetTime();
      PostAlienReceiverFrequencyService(ThisEvent);
    }
    else{
    }
    ReturnVal = true;
  }
  LastInputState = CurrentInputState;
  return ReturnVal;
}
/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

