/****************************************************************************
 Module
   OxygenTimerFSM.c

 Revision
   1.0.1

 Description
   This is a OxygenTimer file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunOxygenTimerSM()
 10/23/11 18:20 jec      began conversion from SMOxygenTimer.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "OxygenTimerFSM.h"
#include "ES_Events.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
/*----------------------------- Module Defines ----------------------------*/
#define BitMaskForPORTD_HI (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT6HI | BIT7HI)
#define ONE_SEC 976
#define TEN_SEC (10*ONE_SEC)
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static OxygenTimerState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitOxygenTimerFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
     Initializes PORTD to be used for the LEDs controlling the oxygen timer
     Notes: Connections are as follows
  1stLED -> PD7
  2ndLED -> PD6
  3rdLED -> PD3
  4thLED -> PD2
  5thLED -> PD1
  6thLED -> PD0
 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitOxygenTimerFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3; //set up port D
  
  while((HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R3)!=SYSCTL_PRGPIO_R3) //wait for peripheral to be ready
  {
  }
  
  HWREG(GPIO_PORTD_BASE+GPIO_O_DEN) |= BitMaskForPORTD_HI; //configure inputs/outputs
  HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) |= BitMaskForPORTD_HI;
  //Initialize all pins to HIGH (All lights are ON by default)
  HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) |= BitMaskForPORTD_HI; 
  
  ES_Timer_InitTimer(O2Timer, TEN_SEC);
  CurrentState = InitOState;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostOxygenTimerFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostOxygenTimerFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunOxygenTimerFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Counts down to a minute at steps of 10seconds. After every 10 seconds,
  one LED goes OFF.
  If it is reset at any time then goes back to the all LEDs lit state.
 Notes
   uses nested switch/case to implement the machine.
  States: AllLEDs_ON, FiveLEDsON, FourLEDsON, ThreeLEDsON,
  TwoLEDsON, OneLEDsON, NoLEDsON
 Author
   J. Edward Carryer, 01/15/12, 15:23

****************************************************************************/
ES_Event_t RunOxygenTimerFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitOState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
      //Initial pseudo state
        CurrentState = AllLEDs_ON;
      }
    }
    break;

    case AllLEDs_ON:        // If current state is state one
    {
     //Reaction to a ResetEvent
      if(ThisEvent.EventType == RESET_EVENT_OCCURED){
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) |= BitMaskForPORTD_HI; 
      }
      
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == O2Timer){
        //Switching OFF first LED
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT7LO;
        CurrentState = FiveLEDsON;
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
    }
    break;
    
    case FiveLEDsON:
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == O2Timer){
         //Switching OFF second LED
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT6LO;
        CurrentState = FourLEDsON;
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
      //Reaction to a ResetEvent
      if(ThisEvent.EventType == RESET_EVENT_OCCURED){
        CurrentState = AllLEDs_ON;
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) |= BitMaskForPORTD_HI; 
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
    }
    break;
    
    case FourLEDsON:
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == O2Timer){
         //Switching OFF third LED
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT3LO;
        CurrentState = ThreeLEDsON;
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
      //Reaction to a ResetEvent
      if(ThisEvent.EventType == RESET_EVENT_OCCURED){
        CurrentState = AllLEDs_ON;
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) |= BitMaskForPORTD_HI; 
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
    }
    break;
    
    case ThreeLEDsON:
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == O2Timer){
        //Switching OFF fourth LED
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT2LO;
        CurrentState = TwoLEDsON;
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
      //Reaction to a ResetEvent
      if(ThisEvent.EventType == RESET_EVENT_OCCURED){
        CurrentState = AllLEDs_ON;
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) |= BitMaskForPORTD_HI; 
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
    }
    break;
    
    case TwoLEDsON:
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == O2Timer){
        //Switching OFF fifth LED
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT1LO;
        CurrentState = OneLEDsON;
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
      //Reaction to a ResetEvent
      if(ThisEvent.EventType == RESET_EVENT_OCCURED){
        CurrentState = AllLEDs_ON;
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) |= BitMaskForPORTD_HI; 
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
    }
    break;
    
    case OneLEDsON:
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == O2Timer){
        //Switching OFF sixth LED
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
        CurrentState = NoLEDsON;
        ES_Event_t NewEvent;
        NewEvent.EventType = USER_HAS_LOST;
        ES_PostAll(NewEvent);
      }
      //Reaction to a ResetEvent
      if(ThisEvent.EventType == RESET_EVENT_OCCURED){
        CurrentState = AllLEDs_ON;
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) |= BitMaskForPORTD_HI; 
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
    }
    break;
    
    case NoLEDsON:
    {
      //Reaction to a ResetEvent
      if(ThisEvent.EventType == RESET_EVENT_OCCURED){
        CurrentState = AllLEDs_ON;
        HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) |= BitMaskForPORTD_HI; 
        ES_Timer_InitTimer(O2Timer, TEN_SEC);
      }
    }
    break;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryOxygenTimerSM

 Parameters
     None

 Returns
     OxygenTimerState_t The current state of the OxygenTimer state machine

 Description
     returns the current state of the OxygenTimer state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
OxygenTimerState_t QueryOxygenTimerFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

