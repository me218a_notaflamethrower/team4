/****************************************************************************

  Header file for OxygenTimer Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMOxygenTimer_H
#define FSMOxygenTimer_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitOState, AllLEDs_ON, FiveLEDsON, FourLEDsON, ThreeLEDsON,
  TwoLEDsON, OneLEDsON, NoLEDsON
}OxygenTimerState_t;

// Public Function Prototypes

bool InitOxygenTimerFSM(uint8_t Priority);
bool PostOxygenTimerFSM(ES_Event_t ThisEvent);
ES_Event_t RunOxygenTimerFSM(ES_Event_t ThisEvent);
OxygenTimerState_t QueryOxygenTimerSM(void);

#endif /* FSMOxygenTimer_H */

