/****************************************************************************

  Header file for AlienReceiverFrequency service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServAlienReceiverFrequency_H
#define ServAlienReceiverFrequency_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitAlienReceiverFrequencyService(uint8_t Priority);
bool PostAlienReceiverFrequencyService(ES_Event_t ThisEvent);
ES_Event_t RunAlienReceiverFrequencyService(ES_Event_t ThisEvent);
bool CheckForRisingEdge(void);
#endif /* ServAlienReceiverFrequency_H */

