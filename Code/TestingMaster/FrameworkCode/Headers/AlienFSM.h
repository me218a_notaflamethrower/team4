/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef AlienFSM_H
#define AlienFSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  AlienInit, AlienUP, AlienDOWN
}AlienState_t;

// Public Function Prototypes

bool InitAlienFSM(uint8_t Priority);
bool PostAlienFSM(ES_Event_t ThisEvent);
ES_Event_t RunAlienFSM(ES_Event_t ThisEvent);
AlienState_t QueryAlienSM(void);

#endif /* FSMTemplate_H */

