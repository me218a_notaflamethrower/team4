/****************************************************************************
 Module
   LEDStripFSM.c

 Revision
   1.0.1

 Description
   This is a LEDStrip file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunLEDStripSM()
 10/23/11 18:20 jec      began conversion from SMLEDStrip.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "LEDStripFSM.h"
#include "ES_Events.h"
#include "MasterFSM.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
/*----------------------------- Module Defines ----------------------------*/
#define MIDDLE_LED_DISPLAY_HIGH BIT3HI
#define UPPER_LED_DISPLAY_HIGH BIT4HI
#define LOWER_LED_DISPLAY_HIGH BIT5HI
#define MIDDLE_LED_DISPLAY_LOW BIT3LO
#define UPPER_LED_DISPLAY_LOW BIT4LO
#define LOWER_LED_DISPLAY_LOW BIT5LO
#define MIDDLE_LED_DISPLAY_HIGH BIT3HI
#define ONE_SEC 976
#define VICTORY_TIME 0.25*ONE_SEC
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
static void TurnUpperLEDON(void);
static void TurnUpperLEDOFF(void);
static void TurnLowerLEDON(void);
static void TurnLowerLEDOFF(void);
static void TurnMiddleLEDON(void);
static void TurnMiddleLEDOFF(void);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static LEDStripState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitLEDStripFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitLEDStripFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitLEDStripState;
  HWREG(SYSCTL_RCGCGPIO) |= BIT4HI;
  while((HWREG(SYSCTL_PRGPIO) & BIT4HI) != BIT4HI)
  {
  }
  HWREG(GPIO_PORTE_BASE+GPIO_O_DEN) |= (UPPER_LED_DISPLAY_HIGH | LOWER_LED_DISPLAY_HIGH |MIDDLE_LED_DISPLAY_HIGH);
  HWREG(GPIO_PORTE_BASE+GPIO_O_DIR) |= (UPPER_LED_DISPLAY_HIGH | LOWER_LED_DISPLAY_HIGH | MIDDLE_LED_DISPLAY_HIGH);
  
  TurnUpperLEDOFF();
  TurnLowerLEDOFF();
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostLEDStripFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostLEDStripFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunLEDStripFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunLEDStripFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitLEDStripState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        CurrentState = BothBlinkingON;
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();
      }
    }
    break;

    case BothBlinkingON:        // If current state is state one
    {
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == LEDStrip_BlinkTimer){
         TurnUpperLEDOFF();
         TurnLowerLEDOFF();
         TurnMiddleLEDOFF();
        CurrentState = BothBlinkingOFF;
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
      }
      if(ThisEvent.EventType == TOT_INSERTED){
        ES_Timer_StopTimer(LEDStrip_BlinkTimer);
        printf("Tot inserted detected at LED_Strip\r\n");
        TurnUpperLEDOFF();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        CurrentState = OnlyLowerON;
      }
    }
    break;
    
    case BothBlinkingOFF:        // If current state is state one
    {
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == LEDStrip_BlinkTimer){
         TurnUpperLEDON();
         TurnLowerLEDON();
         TurnMiddleLEDON();
        CurrentState = BothBlinkingON;
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
      }
      if(ThisEvent.EventType == TOT_INSERTED){
        ES_Timer_StopTimer(LEDStrip_BlinkTimer);
        printf("Tot inserted detected at LED_Strip\r\n");
        TurnUpperLEDOFF();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        CurrentState = OnlyLowerON;
      }
    }
    break;
    
    case OnlyLowerON:
    {
      if(ThisEvent.EventType == Game1CrankCompleted){
        printf("Game 1 crank completed detected by LED strip\r\n");
        TurnUpperLEDON();
        TurnLowerLEDOFF();
        TurnMiddleLEDON();
        CurrentState = OnlyUpperON;
      }
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == ALIEN_TIMER){
        printf("LED strips need to be OFF\r\n");
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        CurrentState = BothOFF_From1;
      }
      if(ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = BothBlinkingON;
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
      }
      if(ThisEvent.EventType == USER_HAS_LOST){
        CurrentState = BothOFF_From1;
        TurnUpperLEDOFF();
        TurnLowerLEDOFF();
        TurnMiddleLEDOFF();
      }
    }
    break;
    
    case BothOFF_From1:
    {
      if(ThisEvent.EventType == ALIEN_SHOT){
        TurnUpperLEDOFF();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        CurrentState = OnlyLowerON;
      }
      if(ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = BothBlinkingON;
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
      }
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == EndGameWaitTimer){
        CurrentState = BothBlinkingON;
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
      }
    }
    break;
    
    case OnlyUpperON:
    {
      if(ThisEvent.EventType == Game2JigsawCompleted){
        printf("Game2 completed detected by LED strips\r\n");
        CurrentState = VictoryBothBlinkingON;
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, VICTORY_TIME);
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();
      }
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == ALIEN_TIMER){
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDOFF();
        CurrentState = BothOFF_From2;
      }
      if(ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = BothBlinkingON;
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
      }
      if(ThisEvent.EventType == USER_HAS_LOST){
        CurrentState = BothOFF_From2;
        TurnUpperLEDOFF();
        TurnLowerLEDOFF();
        TurnMiddleLEDOFF();
      }
    }
    break;
    
    case BothOFF_From2:
    {
      if(ThisEvent.EventType == ALIEN_SHOT){
        TurnUpperLEDON();
        TurnLowerLEDOFF();
        TurnMiddleLEDON();
        CurrentState = OnlyUpperON;
      }
      if(ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = BothBlinkingON;
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
      }
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == EndGameWaitTimer){
        CurrentState = BothBlinkingON;
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
      }
    }
    break;
    
    case VictoryBothBlinkingON:
    {
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == LEDStrip_BlinkTimer){
         TurnUpperLEDOFF();
         TurnLowerLEDOFF();
         TurnMiddleLEDOFF();
         CurrentState = VictoryBothBlinkingOFF;
         ES_Timer_InitTimer(LEDStrip_BlinkTimer, VICTORY_TIME);
      }
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == EndGameWaitTimer){
        CurrentState = BothBlinkingON;
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
        TurnUpperLEDON();
        TurnLowerLEDON(); 
        TurnMiddleLEDON();
      }
    }
    break;
    
    case VictoryBothBlinkingOFF:
    {
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == LEDStrip_BlinkTimer){
         TurnUpperLEDON();
         TurnLowerLEDON();
         TurnMiddleLEDON();
         CurrentState = VictoryBothBlinkingON;
         ES_Timer_InitTimer(LEDStrip_BlinkTimer, VICTORY_TIME);
      }
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == EndGameWaitTimer){
        CurrentState = BothBlinkingON;
        ES_Timer_InitTimer(LEDStrip_BlinkTimer, ONE_SEC);
        TurnUpperLEDON();
        TurnLowerLEDON();
        TurnMiddleLEDON();        
      }
    }
    
    break;
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryLEDStripSM

 Parameters
     None

 Returns
     LEDStripState_t The current state of the LEDStrip state machine

 Description
     returns the current state of the LEDStrip state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
LEDStripState_t QueryLEDStripFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
static void TurnUpperLEDON(void){
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) |= UPPER_LED_DISPLAY_HIGH;
}

static void TurnLowerLEDON(void){
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) |= LOWER_LED_DISPLAY_HIGH;
}

static void TurnUpperLEDOFF(void){
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) &= UPPER_LED_DISPLAY_LOW;
}

static void TurnLowerLEDOFF(void){
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) &= LOWER_LED_DISPLAY_LOW;
}

static void TurnMiddleLEDON(void){
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) |= MIDDLE_LED_DISPLAY_HIGH;
}

static void TurnMiddleLEDOFF(void){
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) &= MIDDLE_LED_DISPLAY_LOW;
}

