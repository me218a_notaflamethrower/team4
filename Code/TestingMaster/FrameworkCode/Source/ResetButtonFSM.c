/****************************************************************************
 Module
   ResetButtonFSM.c

 Revision
   1.0.1

 Description
   This is a ResetButton file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunResetButtonSM()
 10/23/11 18:20 jec      began conversion from SMResetButton.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"
#include "ES_Events.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ResetButtonFSM.h"


/*----------------------------- Module Defines ----------------------------*/
//PC5 used as the reset button pin on the TIVA
#define RESET_BUTTON_PIN_HIGH BIT5HI
#define RESET_BUTTON_PIN_LOW BIT5LO
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static ResetButtonState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority, LastResetButtonState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitResetButtonFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitResetButtonFSM(uint8_t Priority)
{
  MyPriority = Priority;
  // put us into the Initial PseudoState
	HWREG(SYSCTL_RCGCGPIO) |= BIT2HI;
  while((HWREG(SYSCTL_PRGPIO) & BIT2HI) != BIT2HI)
  {
  }
  //Enable and set port C bit 5 to input
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= RESET_BUTTON_PIN_HIGH;
  HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= RESET_BUTTON_PIN_LOW;
  HWREG(GPIO_PORTC_BASE+GPIO_O_PUR) |= RESET_BUTTON_PIN_HIGH;
  //Reading present value of PC5
  LastResetButtonState = HWREG(GPIO_PORTC_BASE+(GPIO_O_DATA + ALL_BITS)) & RESET_BUTTON_PIN_HIGH;
	//Set Current state to debouncing
  CurrentState = Debouncing;
	
	//Start debounce timer
	ES_Timer_InitTimer(ResetButtonDebounce_TIMER, 50);
	
  return true;
 }

/*****************************************************************************
 Check ResetButton Events
 *****************************************************************************/
 
bool CheckResetButtonEvents(void)
{
	bool ReturnVal = false;
	uint8_t CurrentResetButtonState;
	ES_Event_t ThisEvent;
	CurrentResetButtonState = HWREG(GPIO_PORTC_BASE+(GPIO_O_DATA + ALL_BITS)) & RESET_BUTTON_PIN_HIGH;
	
	if(CurrentResetButtonState != LastResetButtonState){
		ReturnVal = true;
		if(CurrentResetButtonState == 0){
      printf("Reset button pressed");
			ThisEvent.EventType = ResetButtonDown;
			PostResetButtonFSM(ThisEvent);
		}
		else{
		}
		LastResetButtonState = CurrentResetButtonState;
	}
	return ReturnVal;
}


/****************************************************************************
 Function
     PostResetButtonFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostResetButtonFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunResetButtonFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunResetButtonFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent, NewEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case Debouncing:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == ResetButtonDebounce_TIMER)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        CurrentState = Ready2Sample;
      }
    }
    break;

    case Ready2Sample:        // If current state is state one
    {
      if(ThisEvent.EventType == ResetButtonDown){
        //Start debounce timer
          ES_Timer_InitTimer(ResetButtonDebounce_TIMER, 50);
          //Set current state to debouncing
          NewEvent.EventType = RESET_EVENT_OCCURRED;
          ES_PostAll(NewEvent);
          CurrentState = Debouncing;
      }
    }
    break;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryResetButtonSM

 Parameters
     None

 Returns
     ResetButtonState_t The current state of the ResetButton state machine

 Description
     returns the current state of the ResetButton state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
ResetButtonState_t QueryResetButtonFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

