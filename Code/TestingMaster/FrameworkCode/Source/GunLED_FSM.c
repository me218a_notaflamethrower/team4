/****************************************************************************
 Module
   GunLED_FSM.c

 Revision
   1.0.1

 Description
   This is a GunLED_ file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunGunLED_SM()
 10/23/11 18:20 jec      began conversion from SMGunLED_.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "GunLED_FSM.h"
#include "ES_Events.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/* include header files for the framework
*/
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
/*----------------------------- Module Defines ----------------------------*/
#define GUN_LED_PIN_HIGH BIT4HI     //using PA4 for the GUN LED
#define GUN_LED_PIN_LOW BIT4LO

#define HALF_TIME_PERIOD 500  //(For 1kHz frequncy) Setting 500micro seconds as half time period
                                      

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
static void WriteGunLED_LOW(void);
static void WriteGunLED_HIGH(void);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static GunLED_State_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitGunLED_FSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitGunLED_FSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitGunState;
  //Initializing PA4 to be output for GUN LED PIN
  HWREG(SYSCTL_RCGCGPIO) |= BIT0HI;
  while((HWREG(SYSCTL_PRGPIO) & BIT0HI) != BIT0HI)
  {
  }
  HWREG(GPIO_PORTA_BASE+GPIO_O_DEN) |= GUN_LED_PIN_HIGH;
  HWREG(GPIO_PORTA_BASE+GPIO_O_DIR) |= GUN_LED_PIN_HIGH;
  //Initialize the value at PA4 to be LOW
  HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA + ALL_BITS)) &= GUN_LED_PIN_LOW;
  
  //Initialize short timer B
  ES_ShortTimerInit(SHORT_TIMER_UNUSED, MyPriority);
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostGunLED_FSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostGunLED_FSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunGunLED_FSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunGunLED_FSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitGunState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        CurrentState = GunLED_OFF;
      }
    }
    break;

    case GunLED_OFF:        // If current state is state one
    {
      if(ThisEvent.EventType == GUN_TRIGGER_PULLED){
        CurrentState = GunLED_PulsingON;
        printf("LED pulsing ON\r\n");
        WriteGunLED_HIGH();
        ES_ShortTimerStart(TIMER_B,HALF_TIME_PERIOD); 
      }
    }
    break;
    
    case GunLED_PulsingON:        // If current state is state one
    {
      if(ThisEvent.EventType == ES_SHORT_TIMEOUT){
        CurrentState = GunLED_PulsingOFF;
        printf("LED pulsing OFF\r\n");
        WriteGunLED_LOW();
        ES_ShortTimerStart(TIMER_B,HALF_TIME_PERIOD); 
      }
//      if(ThisEvent.EventType == ALIEN_SHOT){
//        CurrentState = GunLED_OFF;
//        WriteGunLED_LOW();
//      }
      if(ThisEvent.EventType == USER_HAS_LOST){
        CurrentState = GunLED_OFF;
        WriteGunLED_LOW();
      }
      if(ThisEvent.EventType == RESET_EVENT_OCCURED){
        CurrentState = GunLED_OFF;
        WriteGunLED_LOW();
      }
    }
    break;
    
    case GunLED_PulsingOFF:        // If current state is state one
    {
      if(ThisEvent.EventType == ES_SHORT_TIMEOUT){
        CurrentState = GunLED_PulsingON;
        printf("LED pulsing ON\r\n");
        WriteGunLED_HIGH();
        ES_ShortTimerStart(TIMER_B,HALF_TIME_PERIOD); 
      }
//      if(ThisEvent.EventType == ALIEN_SHOT){
//        CurrentState = GunLED_OFF;
//        WriteGunLED_LOW();
//      }
      if(ThisEvent.EventType == USER_HAS_LOST){
        CurrentState = GunLED_OFF;
        WriteGunLED_LOW();
      }
      if(ThisEvent.EventType == RESET_EVENT_OCCURED){
        CurrentState = GunLED_OFF;
        WriteGunLED_LOW();
      }
    }
    break;
    
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryGunLED_SM

 Parameters
     None

 Returns
     GunLED_State_t The current state of the GunLED_ state machine

 Description
     returns the current state of the GunLED_ state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
GunLED_State_t QueryGunLED_FSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
static void WriteGunLED_LOW(void){
  HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA + ALL_BITS)) &= GUN_LED_PIN_LOW;
  return;
}

static void WriteGunLED_HIGH(void){
  HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA + ALL_BITS)) |= GUN_LED_PIN_HIGH;
  return;
}
