/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "AlienFSM.h"
#include "ES_Events.h"
#include "ADMulti.h"
#include "PWM16Tiva.h"
#include "MasterFSM.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/*----------------------------- Module Defines ----------------------------*/
#define ALIEN_SERVO_PWM_PIN BIT4HI //pin on port B to actuate the servo that controls the alien
#define ALIEN_SERVO_CHANNEL 2
#define SERVO_PERIOD 25000 //20 ms in ticks
#define SERVO_DOWN 900 //3100 for small
#define SERVO_UP 2400 //2400 for small

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
bool InitAlienFSM(uint8_t Priority);
bool PostAlienFSM(ES_Event_t ThisEvent);
ES_Event_t RunAlienFSM(ES_Event_t ThisEvent);
AlienState_t QueryAlienSM(void);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static AlienState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitAlienFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitAlienFSM(uint8_t Priority)
{
  //Initialize alien servo pin as a pwm pin (NOT NECESSARY IF 3 PWMs DECLARED ELSEWHERE)
//  PWM_TIVA_Init(3);
  
  //Initialize the ALIEN_SERVO_PWM_PIN for the alien to be down
  
  PWM_TIVA_SetPeriod(SERVO_PERIOD, 1);  //group 1 encompasses TOT and Alien servo
  PWM_TIVA_SetPulseWidth(SERVO_DOWN ,ALIEN_SERVO_CHANNEL);
  printf("Alien servo should be down");
  //Initialize CurrentState to AlienInit
  
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = AlienInit;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostAlienFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostAlienFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunAlienFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunAlienFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  switch (CurrentState)
  {
    case AlienInit:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        CurrentState = AlienDOWN;
        printf("Alien is DOWN");
      }
    }
    break;

    case AlienDOWN:    
    {
     
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == ALIEN_TIMER && QueryMasterFSM() != Welcome)
      {
         printf("Alien timeout has occurred");
        PWM_TIVA_SetPulseWidth(SERVO_UP,ALIEN_SERVO_CHANNEL); // move alien to 'up' position
        CurrentState = AlienUP;
      }
    }
    break;
    
    case AlienUP:       
    printf("UP");    {
      if (ThisEvent.EventType == ALIEN_SHOT)
      {
        printf("Alien shot detected in Alien_FSM");
        PWM_TIVA_SetPulseWidth(SERVO_DOWN,ALIEN_SERVO_CHANNEL); // move alien to 'down' position
        CurrentState = AlienDOWN;
      }
      if (ThisEvent.EventType == USER_HAS_LOST)
      {
        PWM_TIVA_SetPulseWidth(SERVO_DOWN,ALIEN_SERVO_CHANNEL); // move alien to 'down' position
        CurrentState = AlienDOWN;
      }
      if (ThisEvent.EventType == RESET_EVENT_OCCURRED)
      {
        PWM_TIVA_SetPulseWidth(SERVO_DOWN,ALIEN_SERVO_CHANNEL); // move alien to 'down' position
        CurrentState = AlienDOWN;
      }
    }
    break;
    
    default:
      ;
  }                          // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryAlienSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
AlienState_t QueryAlienFSM(void)
{
  return CurrentState;
}




/***************************************************************************
 private functions
 ***************************************************************************/

