/****************************************************************************

  Header file for Game1Crank Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMGame1Crank_H
#define FSMGame1Crank_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitState, All_LEDs_OFF, Only1LED_ON,
  LEDs1to2ON, LEDs1to3ON, LEDs1to4ON, LEDs1to5ON,
  LED6_ON_BLINKING, LED6_OFF_BLINKING, AllLEDsON
}Game1CrankState_t;

// Public Function Prototypes

bool InitGame1CrankFSM(uint8_t Priority);
bool PostGame1CrankFSM(ES_Event_t ThisEvent);
ES_Event_t RunGame1CrankFSM(ES_Event_t ThisEvent);
Game1CrankState_t QueryGame1CrankSM(void);
bool CheckCrankVoltage(void);

#endif /* FSMGame1Crank_H */

