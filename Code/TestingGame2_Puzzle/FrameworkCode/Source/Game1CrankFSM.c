/****************************************************************************
 Module
   Game1CrankFSM.c

 Revision
   1.0.1

 Description
   This is a Game1Crank file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunGame1CrankSM()
 10/23/11 18:20 jec      began conversion from SMGame1Crank.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Game1CrankFSM.h"
#include "ES_Events.h"
#include "ShiftRegisterWrite.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
/*----------------------------- Module Defines ----------------------------*/
//#define CRANK_MOTOR_PIN XX    //Analog input pin of the TIVA
/*All the voltage thresholds
#define FIRST_THRESHOLD XX
#define SECOND_THRESHOLD XX
#define THRID_THRESHOLD XX
#define FOURTH_THRESHOLD XX
#define FIFTH_THRESHOLD XX
#define SIXTH_THRESHOLD XX
*/
/*Constants that need to be written to the shift register to ensure the 
desired number of LEDs are lit*/
/*
#define LIGHT0LED 0
#define LIGHT1LED 1
#define LIGHT2LED 3
#define LIGHT3LED 7
#define LIGHT4LED 15
#define LIGHT5LED 31
#define LIGHT6LED 63
*/
static uint8_t LIGHT0LED;
static uint8_t LIGHT1LED ;
static uint8_t LIGHT2LED ;
static uint8_t LIGHT3LED ;
static uint8_t LIGHT4LED ;
static uint8_t LIGHT5LED ;
static uint8_t LIGHT6LED ;

#define PulsingTime 976
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static Game1CrankState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitGame1CrankFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitGame1CrankFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitState;
  //SR_Write(LIGHT0LED);
  LIGHT0LED = 0;
  LIGHT1LED = 1;
  LIGHT2LED = 3;
  LIGHT3LED = 7;
  LIGHT4LED = 15;
  LIGHT5LED = 31;
  LIGHT6LED = 63;
  SR_Init();
  printf("Initialized\r\n");
  // post the initial transition event
  ES_Timer_InitTimer(LED6_BlinkTimer, PulsingTime);
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostGame1CrankFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostGame1CrankFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunGame1CrankFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunGame1CrankFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        SR_Write(LIGHT0LED);
        CurrentState = All_LEDs_OFF;
      }
    }
    break;

    case All_LEDs_OFF:        // If current state is state one
    {
      //printf("All LEDs need to be OFF");
      if (ThisEvent.EventType == FIRST_THRESHOLD_CROSSED){
        printf("First threshold crossed event\r\n");
        SR_Write(LIGHT1LED);
        //HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT0HI);
        printf("Only 1 LED needs to be ON");
        CurrentState = Only1LED_ON;
    }
    break;
    }
    
    case Only1LED_ON:
    {
      if (ThisEvent.EventType == SECOND_THRESHOLD_CROSSED){
        SR_Write(LIGHT2LED);
        printf("Only 2 LEDs need to be ON");
        CurrentState = LEDs1to2ON;
      }
    }
    break;
    
    case LEDs1to2ON:
    {      
      if (ThisEvent.EventType == THIRD_THRESHOLD_CROSSED){
        SR_Write(LIGHT3LED);
        CurrentState = LEDs1to3ON;
      }
    }
    break;
    
    case LEDs1to3ON:
    {
      if (ThisEvent.EventType == FOURTH_THRESHOLD_CROSSED){
        SR_Write(LIGHT4LED);
        CurrentState = LEDs1to4ON;
      }
    }
    break;
    
    case LEDs1to4ON:
    {
      if (ThisEvent.EventType == FIFTH_THRESHOLD_CROSSED){
        SR_Write(LIGHT5LED);
        CurrentState = LEDs1to5ON;
      }
    }
    break;
    
    case LEDs1to5ON:
    {
      if (ThisEvent.EventType == SIXTH_THRESHOLD_CROSSED){
        SR_Write(LIGHT6LED);
        CurrentState = LED6_ON_BLINKING;
        printf("LED6 should be ON\r\n");
        ES_Timer_InitTimer(LED6_BlinkTimer, PulsingTime);
      }
    }
    break;
    
    case LED6_ON_BLINKING:
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == LED6_BlinkTimer){
        SR_Write(LIGHT5LED);
        CurrentState = LED6_OFF_BLINKING;
        printf("LED6 should be OFF\r\n");
        ES_Timer_InitTimer(LED6_BlinkTimer, PulsingTime);
      }
    }
    break;
    
    case LED6_OFF_BLINKING:
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == LED6_BlinkTimer){
        SR_Write(LIGHT6LED);
        CurrentState = LED6_ON_BLINKING;
        printf("LED6 should be ON\r\n");
        ES_Timer_InitTimer(LED6_BlinkTimer, PulsingTime);
      }
    }
    break;
    
    case AllLEDsON:
    {
    }
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryGame1CrankSM

 Parameters
     None

 Returns
     Game1CrankState_t The current state of the Game1Crank state machine

 Description
     returns the current state of the Game1Crank state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
Game1CrankState_t QueryGame1CrankFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/


