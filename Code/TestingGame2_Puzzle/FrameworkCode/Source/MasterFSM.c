/****************************************************************************
 Module
   MasterFSM.c

 Revision
   1.0.1

 Description
   This is a Master file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunMasterSM()
 10/23/11 18:20 jec      began conversion from SMMaster.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MasterFSM.h"
#include "ES_Events.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
/*----------------------------- Module Defines ----------------------------*/
#define TOT_SENSOR_PIN_HIGH BIT4HI  //PC4 HIGH
#define TOT_SENSOR_PIN_LOW BIT4LO //PC4 LOW
#define TOT_SERVO_PIN_HIGH XX
#define TOT_SERVO_PIN_LOW XX
#define FENCE_DISPLAY_LED_PIN_HIGH XX
#define FENCE_DISPLAY_LED_PIN_LOW XX
#define PUZZLE_DISPLAY_LED_PIN_HIGH XX
#define PUZZLE_DISPLAY_LED_PIN_LOW XX

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
bool CheckTOTStatus(void);
static uint8_t GetTOTSensorPinState(void);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static MasterState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority, LastTOT_SensorVal;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMasterFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitMasterFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = MasterInitState;
  //Enable and set port C bit 4 to input with PullDown for TOT_SENSOR_PIN
  HWREG(SYSCTL_RCGCGPIO) |= BIT2HI;
  while((HWREG(SYSCTL_PRGPIO) & BIT2HI) != BIT2HI)
  {
  }
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= TOT_SENSOR_PIN_HIGH;
  HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= TOT_SENSOR_PIN_LOW;
  HWREG(GPIO_PORTC_BASE+GPIO_O_PDR) |= TOT_SENSOR_PIN_HIGH; //Pulldown enable so it is LOW by default
  
  LastTOT_SensorVal = GetTOTSensorPinState();
  
  //Code for initializing TOT_SERVO_PIN
  
  //Code for initializing FENCE_DISPLAY_LED_PIN
  
  //Code for initializing PUZZLE_DISPLAY_LED_PIN
  
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMasterFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMasterFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMasterFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMasterFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case MasterInitState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        CurrentState = Welcome;
        /***********************
        Insert code to perform LED welcome sequence
        ************************/
      }
    }
    break;

    case Welcome:        // If current state is state one
    {
     if (ThisEvent.EventType == TOT_INSERTED){
       CurrentState = PlayingGame1Crank;
       /*****************insert code for the following******
       //Pull FENCE_DISPLAY_LED_PIN HIGH
       //Init Alien timer
       //Init NoUserInput timer
       *****************************************************/
     }
    }
    break;
    
    case PlayingGame1Crank:
    {
      if (ThisEvent.EventType == RESET_EVENT_OCCURED){
        CurrentState = Welcome;
         /***********************
        Insert code to perform LED welcome sequence
        //Pull FENCE_DISPLAY_LED_PIN LOW
        ************************/
      }
      if (ThisEvent.EventType == USER_HAS_LOST){
        CurrentState = Defeated;
        /***********************Insert code for the following:
        //Pull FENCE_DISPLAY_LED_PIN LOW
        //Init GameEndWaitTimer
        //Write TOT_SERVO_PIN to open
        ************************/
      }
     /* if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == NoUserInputTimer){
        CurrentState = Welcome;
         ***********************
        Insert code to perform LED welcome sequence
        //Pull FENCE_DISPLAY_LED_PIN LOW
        }
        ************************/
   
     }
     break; 
    //end switch case
    }
    
    // repeat state pattern as required for other states
    
  return ReturnEvent;
  }                                   // end switch on Current State

/****************************************************************************
 Function
     QueryMasterSM

 Parameters
     None

 Returns
     MasterState_t The current state of the Master state machine

 Description
     returns the current state of the Master state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
MasterState_t QueryMasterFSM(void)
{
  return CurrentState;
}

/****************************************************************************
 Function
     CheckTOTStatus

 Parameters
     None

 Returns
     returns true if event is detected with the TOT

 Description
     Posts TOT_INSERTED or RESET_EVENT_OCCURED if there is any event detected
with respect to the TOT
 Notes

 Author
     Pranav Keni, 10/23/11, 19:21
****************************************************************************/
bool CheckTOTStatus(void){
  uint8_t CurrentTOTStatus = GetTOTSensorPinState();
  bool ReturnVal = false;
  ES_Event_t ThisEvent;
  if (CurrentTOTStatus != LastTOT_SensorVal){
    if(CurrentTOTStatus == 0){
      printf("TOT inserted event\r\n");
      ReturnVal = true;
      ThisEvent.EventType = TOT_INSERTED;
      PostMasterFSM(ThisEvent);
    }
    else{
      printf("TOT removed event\r\n");
      ReturnVal = true;
      ThisEvent.EventType = RESET_EVENT_OCCURED;
      ES_PostAll(ThisEvent);
      
    }
  }
  LastTOT_SensorVal = CurrentTOTStatus;
  return ReturnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/
static uint8_t GetTOTSensorPinState(void){
  uint8_t value;
  value = HWREG(GPIO_PORTC_BASE+(GPIO_O_DATA + ALL_BITS)) & TOT_SENSOR_PIN_HIGH;
  return value;
}
