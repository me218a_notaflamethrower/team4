/****************************************************************************
 Module
   Game2PuzzleFSM.c

 Revision
   1.0.1

 Description
   This is a Game2Puzzle file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunGame2PuzzleSM()
 10/23/11 18:20 jec      began conversion from SMGame2Puzzle.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Game2PuzzleFSM.h"
#include "ES_Events.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
/*----------------------------- Module Defines ----------------------------*/
#define ALL_SENSOR_BITS_HIGH (BIT2HI | BIT3HI | BIT4HI | BIT5HI | BIT6HI | BIT7HI)
#define ALL_SENSOR_BITS_LOW (BIT2LO & BIT3LO & BIT4LO & BIT5LO & BIT6LO & BIT7LO)
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
bool CheckIfPuzzleSolved(void);
static uint8_t GetStateOfAllPins(void);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static Game2PuzzleState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;
static bool IsPuzzleSolved;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitGame2PuzzleFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitGame2PuzzleFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitG2State;
	//Initialize the pin(s) needed for signal from the ambient phototransistors
	HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0; //set up port A
  
  while((HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R0)!=SYSCTL_PRGPIO_R0) //wait for peripheral to be ready
  {
  }
  
  HWREG(GPIO_PORTA_BASE+GPIO_O_DEN) |= (ALL_SENSOR_BITS_HIGH); //using PB3 as our line on the TIVA
  HWREG(GPIO_PORTA_BASE+GPIO_O_DIR) &= (ALL_SENSOR_BITS_LOW);
	//Initialize current state of the puzzle to be unsolved
	IsPuzzleSolved = false;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostGame2PuzzleFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostGame2PuzzleFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunGame2PuzzleFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunGame2PuzzleFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitG2State:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        CurrentState = WaitingForGame1;
      }
    }
    break;

    case WaitingForGame1:        // If current state is state one
    {
      printf("In Waiting for Game1\r\n");
      if (ThisEvent.EventType == Game1CrankCompleted){
					CurrentState = PuzzleUnsolvedPlaying;
          printf("Going to Puzzle unsolved and waiting\r\n");
      }
    }
    break;
		
		
		case PuzzleUnsolvedPlaying:        // If current state is state one
    {
      printf("In Puzzle unsolved and waiting\r\n");
      if (ThisEvent.EventType == USER_HAS_LOST){
					CurrentState = WaitingForGame1;
			}
//			if (ThisEvent.EventType == RESET_EVENT_OCCURED){
//					CurrentState = WaitingForGame1;
//			}
			if (ThisEvent.EventType == PUZZLE_SOLVED){ //&& QueryMasterFSM == PlayingGame2Jigsaw){
        printf("Puzzle has been solved\r\n");
				//PostMasterFSM(Game2JigsawCompleted);	
				CurrentState = PuzzleSolved;
			}
    }
    break;
		
		case PuzzleSolved:        // If current state is state one
    {
//			if (ThisEvent.EventType == RESET_EVENT_OCCURED){
//					CurrentState = WaitingForGame1;
//          IsPuzzleSolved = false;
//			}
    }
    break;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryGame2PuzzleSM

 Parameters
     None

 Returns
     Game2PuzzleState_t The current state of the Game2Puzzle state machine

 Description
     returns the current state of the Game2Puzzle state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
Game2PuzzleState_t QueryGame2PuzzleFSM(void)
{
  return CurrentState;
}

/****************************************************************************
 Function
     CheckIfPuzzleSolved

 Parameters
     None

 Returns
     True if event detected

 Description
     Checks if puzzle is solved
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
bool CheckIfPuzzleSolved(void){
  bool ReturnVal = false;
  uint8_t CurrentPinState; //variable for state of the receiver pin
  CurrentPinState = GetStateOfAllPins(); //Function to get value of the pin (PB3)
  if (CurrentPinState == 0 && IsPuzzleSolved == false){ //Assuming the pin is LOW when puzzle is solved
    ES_Event_t ThisEvent;
    ThisEvent.EventType = PUZZLE_SOLVED; 
    printf("Puzzle solved event detected\r\n");
    PostGame2PuzzleFSM(ThisEvent);
    IsPuzzleSolved = true;
    ReturnVal = true;
  }
  return ReturnVal;
}
/***************************************************************************
 private functions
 ***************************************************************************/
static uint8_t GetStateOfAllPins(void){
   return HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (ALL_SENSOR_BITS_HIGH);
}

