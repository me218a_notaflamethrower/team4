/****************************************************************************

  Header file for Master Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMMaster_H
#define FSMMaster_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  MasterInitState, Welcome, PlayingGame1Crank,
  AlienAttackDuringGame1, PlayingGame2Jigsaw, AlienAttackDuringGame2,
  Victorious, Defeated
}MasterState_t;

// Public Function Prototypes

bool InitMasterFSM(uint8_t Priority);
bool PostMasterFSM(ES_Event_t ThisEvent);
ES_Event_t RunMasterFSM(ES_Event_t ThisEvent);
MasterState_t QueryMasterSM(void);
bool CheckTOTStatus(void);

#endif /* FSMMaster_H */

