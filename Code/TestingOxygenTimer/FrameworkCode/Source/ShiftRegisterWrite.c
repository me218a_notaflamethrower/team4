/****************************************************************************
 Module
   ShiftRegisterWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to a write only shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     first pass
 
****************************************************************************/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"

// readability defines
#define DATA GPIO_PIN_0

#define SCLK GPIO_PIN_1
#define SCLK_HI BIT1HI
#define SCLK_LO BIT1LO

#define RCLK GPIO_PIN_2
#define RCLK_LO BIT2LO
#define RCLK_HI BIT2HI

#define GET_MSB_IN_LSB(x) ((x & 0x80)>>7)
#define ALL_BITS (0xff<<2)

// an image of the last 8 bits written to the shift register
static uint8_t LocalRegisterImage=0;

// Initialize SR (set up port B by enabling peripheral clock, wait for peripheral to be ready, set PB0-2 to output)
void SR_Init(void){
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1; //set up port B
  
  while((HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R1)!=SYSCTL_PRGPIO_R1) //wait for peripheral to be ready
  {
  }
  
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI); //configure inputs/outputs
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT2HI);
  
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO); //data low
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT1LO); //serial clock low
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT2HI); //register clock high

}

// Return data currently on register
uint8_t SR_GetCurrentRegister(void){
  return LocalRegisterImage;
}

// Write NewValue to register
void SR_Write(uint8_t NewValue){

  LocalRegisterImage = NewValue; // save a local copy

// lower the register clock
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT2LO); //register clock low
	int i = 1;
	uint8_t msb = 0;
// shift out the data while pulsing the serial clock  
  for (i=1; i<=8; i++)
  {
		// Isolate the MSB of NewValue, put it into the LSB position and output to port
		msb = GET_MSB_IN_LSB(NewValue); //shift MSB to LSB
		
		if (msb == 0) { //set bit on PB0
			HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO); }
		else {
			HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT0HI); }
			
		
		
		HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI); //raise SCLK
		HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT1LO); //lower SCLK
		
		NewValue = NewValue<<1; //bit shift left

  }
	
	HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT2HI); //register clock high
}
