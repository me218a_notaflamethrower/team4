/****************************************************************************
 Module
   MasterFSM.c

 Revision
   1.0.1

 Description
   This is a Master file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunMasterSM()
 10/23/11 18:20 jec      began conversion from SMMaster.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MasterFSM.h"
#include "ES_Events.h"
#include "PWM16Tiva.h"
#include "OxygenTimerFSM.h"
#include "Game1CrankFSM.h"
#include "LEDStripFSM.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
/*----------------------------- Module Defines ----------------------------*/
#define TOT_SENSOR_PIN_HIGH BIT4HI  //PC4 HIGH
#define TOT_SENSOR_PIN_LOW BIT4LO //PC4 LOW
#define TOT_CLOSED 3000//3000
#define TOT_OPEN 2000
#define TOT_SERVO_CHANNEL 1
#define SERVO_PERIOD 2500


#define NUM_PWM_PINS 4
#define PULSE_FREQUENCY 80
#define LED_GUN_GROUP 0
#define LED_STRIP_GROUP 2

#define LED_STRIP_DUTY_CYCLE 50
#define GUN_LED_DUTY_CYCLE 50
#define GUN_LED_PWMPIN 0

#define FENCE_DISPLAY_PWM_PIN 4
#define Jigsaw_DISPLAY_PWM_PIN 5

#define ONE_SEC 976
#define FIRST_ALIEN_TIME 6*ONE_SEC	//Time for first alien appearance 
#define SECOND_ALIEN_TIME 15*ONE_SEC	//Time for second alien after first is shot
#define RESET_WAIT_TIME ONE_SEC
#define GAME_OVER_WAIT_TIME 5*ONE_SEC
#define USER_INPUT_WAIT_TIME 30*ONE_SEC
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
bool CheckTOTStatus(void);
static uint8_t GetTOTSensorPinState(void);
static void CloseTOT_Servo(void);
static void OpenTOT_Servo(void);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static MasterState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority, LastTOT_SensorVal;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMasterFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitMasterFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = MasterInitState;
  //Enable and set port C bit 4 to input with PullDown for TOT_SENSOR_PIN
  HWREG(SYSCTL_RCGCGPIO) |= BIT2HI;
  while((HWREG(SYSCTL_PRGPIO) & BIT2HI) != BIT2HI)
  {
  }
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= TOT_SENSOR_PIN_HIGH;
  HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= TOT_SENSOR_PIN_LOW;
  HWREG(GPIO_PORTC_BASE+GPIO_O_PDR) |= TOT_SENSOR_PIN_HIGH; //Pulldown enable so it is LOW by default
  
	//Reading TOT_SENSOR_PIN
  LastTOT_SensorVal = GetTOTSensorPinState();
  
  //Code for setting up PWM pins 
	//Gun LED
	PWM_TIVA_Init(NUM_PWM_PINS);  
  PWM_TIVA_SetDuty(GUN_LED_DUTY_CYCLE, GUN_LED_PWMPIN);
	PWM_TIVA_SetFreq(PULSE_FREQUENCY,LED_GUN_GROUP);
  
  //TOT Servo
  PWM_TIVA_SetPulseWidth(TOT_CLOSED ,TOT_SERVO_CHANNEL);

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMasterFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMasterFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMasterFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMasterFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent, NewEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case MasterInitState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        CurrentState = Welcome;
      }
    }
    break;

    case Welcome:        // If current state is state one
    {
     if (ThisEvent.EventType == TOT_INSERTED){
       CurrentState = PlayingGame1Crank;
       printf("In PlayingGame1Crank\r\n");
       //Init Alien timer
			 ES_Timer_InitTimer(ALIEN_TIMER, FIRST_ALIEN_TIME);
       //Init NoUserInput timer
       ES_Timer_InitTimer(NoUserInputTimer, USER_INPUT_WAIT_TIME); 
     }
		 if (ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = Welcome;
        OpenTOT_Servo();
				//Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, RESET_WAIT_TIME);
				//Init EndGameWaitTimer
        ES_Timer_StopTimer(ALIEN_TIMER);
      }
     if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == EndGameWaitTimer){
          CloseTOT_Servo();  //Write TOT_SERVO_PIN to closed - Accouting for when reset button is pressed
      }
    }
    break;
    
    case PlayingGame1Crank:
    {
			if (ThisEvent.EventType == Game1CrankCompleted){
				CurrentState = PlayingGame2Jigsaw;
        printf("In PlayingGame2Jigsaw\r\n");
        ES_Timer_InitTimer(NoUserInputTimer, 40*ONE_SEC);
        ES_Timer_InitTimer(ALIEN_TIMER, SECOND_ALIEN_TIME);
			}
			if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == ALIEN_TIMER){
				CurrentState = AlienAttackDuringGame1;
        printf("In AlienAttackDuringGame1\r\n");
			}
      if (ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = Welcome;
        OpenTOT_Servo();
				//Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, RESET_WAIT_TIME);
				//Init EndGameWaitTimer
        ES_Timer_StopTimer(ALIEN_TIMER);
      }
      if (ThisEvent.EventType == USER_HAS_LOST){
        printf("In Defeated\r\n");
        CurrentState = Defeated;
        OpenTOT_Servo(); //Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, GAME_OVER_WAIT_TIME);  //Init GameEndWaitTimer
        ES_Timer_StopTimer(ALIEN_TIMER);
        ES_Timer_StopTimer(NoUserInputTimer);
      }
     if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == NoUserInputTimer){
        CurrentState = Welcome;
        printf("No user input timer has timed out\r\n");
        NewEvent.EventType = RESET_EVENT_OCCURRED;
        ES_PostAll(NewEvent);
        OpenTOT_Servo();
        ES_Timer_InitTimer(EndGameWaitTimer, RESET_WAIT_TIME);
        ES_Timer_StopTimer(ALIEN_TIMER);
      }
     }
     break;

		case AlienAttackDuringGame1:
		{
			if (ThisEvent.EventType == ALIEN_SHOT){
				CurrentState = PlayingGame1Crank;
        printf("In PlayingGame1Crank\r\n");
        ES_Timer_InitTimer(NoUserInputTimer, USER_INPUT_WAIT_TIME);
			}
			 if (ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = Welcome;
        OpenTOT_Servo();
				//Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, RESET_WAIT_TIME);
        ES_Timer_StopTimer(ALIEN_TIMER);
				//Init EndGameWaitTimer
      }
      if (ThisEvent.EventType == USER_HAS_LOST){
       printf("In Defeated\r\n");
        CurrentState = Defeated;
        OpenTOT_Servo(); //Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, GAME_OVER_WAIT_TIME);  //Init GameEndWaitTimer
        ES_Timer_StopTimer(ALIEN_TIMER);
        ES_Timer_StopTimer(NoUserInputTimer);
      }
     if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == NoUserInputTimer){
        CurrentState = Welcome;
        ES_Timer_StopTimer(ALIEN_TIMER);
        NewEvent.EventType = RESET_EVENT_OCCURRED;
        ES_PostAll(NewEvent);
        OpenTOT_Servo();
        ES_Timer_InitTimer(EndGameWaitTimer, RESET_WAIT_TIME);
     }
		}
		break;
		
		case PlayingGame2Jigsaw:
		{
			if (ThisEvent.EventType == Game2JigsawCompleted){
				CurrentState = Victorious;
        printf("In Victorious\r\n");
        OpenTOT_Servo();  //Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, GAME_OVER_WAIT_TIME); // Init GameEndWaitTimer
        NewEvent.EventType = GAME_WON;
        ES_PostAll(NewEvent);
        ES_Timer_StopTimer(ALIEN_TIMER);
        ES_Timer_StopTimer(NoUserInputTimer);
			}
			if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == ALIEN_TIMER){
				CurrentState = AlienAttackDuringGame2;
        printf("In AlienAttackDuringGame2\r\n");
			}
      if (ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = Welcome;
        OpenTOT_Servo();
				//Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, RESET_WAIT_TIME);
        ES_Timer_StopTimer(ALIEN_TIMER);
				//Init EndGameWaitTimer
      }
      if (ThisEvent.EventType == USER_HAS_LOST){
        printf("In Defeated\r\n");
        CurrentState = Defeated;
        OpenTOT_Servo(); //Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, GAME_OVER_WAIT_TIME);  //Init GameEndWaitTimer
        ES_Timer_StopTimer(ALIEN_TIMER);
        ES_Timer_StopTimer(NoUserInputTimer);
      }
     if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == NoUserInputTimer){
        CurrentState = Welcome;
        NewEvent.EventType = RESET_EVENT_OCCURRED;
        ES_PostAll(NewEvent);
        OpenTOT_Servo();
        ES_Timer_InitTimer(EndGameWaitTimer, RESET_WAIT_TIME);
        ES_Timer_StopTimer(ALIEN_TIMER);
     }
		}
		break;
		
		case AlienAttackDuringGame2:
		{
			if (ThisEvent.EventType == ALIEN_SHOT){
				CurrentState = PlayingGame2Jigsaw;
        printf("In PlayingGame2Jigsaw\r\n");
				//OnlyJigsawDisplayON();
        ES_Timer_InitTimer(NoUserInputTimer, USER_INPUT_WAIT_TIME); 
        ES_Event_t NewEvent;
        NewEvent.EventType = ReturnedFromAlienToGame2;
        PostGame1CrankFSM(NewEvent);
			}
			 if (ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = Welcome;
        OpenTOT_Servo();
				//Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, RESET_WAIT_TIME);
        ES_Timer_StopTimer(ALIEN_TIMER);
				//Init EndGameWaitTimer
      }
      if (ThisEvent.EventType == USER_HAS_LOST){
        printf("In Defeated\r\n");
        CurrentState = Defeated;
        OpenTOT_Servo(); //Write TOT_SERVO_PIN to open
        ES_Timer_InitTimer(EndGameWaitTimer, GAME_OVER_WAIT_TIME);  //Init GameEndWaitTimer
        ES_Timer_StopTimer(ALIEN_TIMER);
        ES_Timer_StopTimer(NoUserInputTimer);
      }
     if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == NoUserInputTimer){
        CurrentState = Welcome;
        NewEvent.EventType = RESET_EVENT_OCCURRED;
        ES_PostAll(NewEvent);
        OpenTOT_Servo();
        ES_Timer_InitTimer(EndGameWaitTimer, RESET_WAIT_TIME);
        ES_Timer_StopTimer(ALIEN_TIMER);
     }
		}
		break;

		case Victorious:
		{
			if (ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = Welcome;
        CloseTOT_Servo(); //Write TOT_SERVO_PIN to close
      }
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == EndGameWaitTimer){
        CurrentState = Welcome;
        CloseTOT_Servo();   //Write TOT_SERVO_PIN to closed 	
      }
		}
		break;
		
		case Defeated:
		{
			if (ThisEvent.EventType == RESET_EVENT_OCCURRED){
        CurrentState = Welcome;
        CloseTOT_Servo();
				//Write TOT_SERVO_PIN to closed
      }
			if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == EndGameWaitTimer){
        CurrentState = Welcome;
        CloseTOT_Servo();   //Write TOT_SERVO_PIN to closed 	
      }
		}
		break;			
    //end switch case
   }
    
    // repeat state pattern as required for other states
    
  return ReturnEvent;
  }                                   // end switch on Current State

/****************************************************************************
 Function
     QueryMasterSM

 Parameters
     None

 Returns
     MasterState_t The current state of the Master state machine

 Description
     returns the current state of the Master state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
MasterState_t QueryMasterFSM(void)
{
  return CurrentState;
}

/****************************************************************************
 Function
     CheckTOTStatus

 Parameters
     None

 Returns
     returns true if event is detected with the TOT

 Description
     Posts TOT_INSERTED or RESET_EVENT_OCCURRED if there is any event detected
with respect to the TOT
 Notes

 Author
     Pranav Keni, 10/23/11, 19:21
****************************************************************************/
bool CheckTOTStatus(void){
  uint8_t CurrentTOTStatus = GetTOTSensorPinState();
  bool ReturnVal = false;
  ES_Event_t ThisEvent;
  if (CurrentTOTStatus != LastTOT_SensorVal){
    if(CurrentTOTStatus == 0){
      printf("TOT inserted event\r\n");
      ReturnVal = true;
      ThisEvent.EventType = TOT_INSERTED;
      PostMasterFSM(ThisEvent);
      PostLEDStripFSM(ThisEvent);
      PostOxygenTimerFSM(ThisEvent);
    }
    else{
    }
  }
  LastTOT_SensorVal = CurrentTOTStatus;
  return ReturnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/
static uint8_t GetTOTSensorPinState(void){
  uint8_t value;
  value = HWREG(GPIO_PORTC_BASE+(GPIO_O_DATA + ALL_BITS)) & TOT_SENSOR_PIN_HIGH;
  return value;
}

static void CloseTOT_Servo(void){
  PWM_TIVA_SetPulseWidth(TOT_CLOSED ,TOT_SERVO_CHANNEL);
}
static void OpenTOT_Servo(void){
  PWM_TIVA_SetPulseWidth(TOT_OPEN ,TOT_SERVO_CHANNEL);
}
