/****************************************************************************

  Header file for Game2Puzzle Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMGame2Puzzle_H
#define FSMGame2Puzzle_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitG2State, WaitingForGame1, PuzzleUnsolvedPlaying, PuzzleSolved
} Game2PuzzleState_t;

// Public Function Prototypes

bool InitGame2PuzzleFSM(uint8_t Priority);
bool PostGame2PuzzleFSM(ES_Event_t ThisEvent);
ES_Event_t RunGame2PuzzleFSM(ES_Event_t ThisEvent);
Game2PuzzleState_t QueryGame2PuzzleSM(void);
bool CheckIfPuzzleSolved(void);
#endif /* FSMGame2Puzzle_H */

