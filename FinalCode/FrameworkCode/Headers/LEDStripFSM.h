/****************************************************************************

  Header file for LEDStrip Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMLEDStrip_H
#define FSMLEDStrip_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitLEDStripState, BothBlinkingON, BothBlinkingOFF,
  OnlyUpperON, BothOFF_From1, BothOFF_From2, OnlyLowerON,
  VictoryBothBlinkingON, VictoryBothBlinkingOFF
}LEDStripState_t;

// Public Function Prototypes

bool InitLEDStripFSM(uint8_t Priority);
bool PostLEDStripFSM(ES_Event_t ThisEvent);
ES_Event_t RunLEDStripFSM(ES_Event_t ThisEvent);
LEDStripState_t QueryLEDStripSM(void);

#endif /* FSMLEDStrip_H */

